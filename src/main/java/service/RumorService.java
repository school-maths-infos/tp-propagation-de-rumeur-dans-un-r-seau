/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package service;

import model.Metrics;
import model.Rumor;
import model.SocialNetwork;
import org.graphstream.graph.Node;
import spreaders.CentralityRumorSpread;
import spreaders.PreferencesRumorSpread;
import spreaders.RumorSpread;
import spreaders.SimpleRumorSpread;
import utils.Constant;

import java.util.Iterator;

public class RumorService {

    private SocialNetwork graph;
    private Metrics metrics = new Metrics();
    private RumorSpread simpleRumorSpread = new SimpleRumorSpread();
    private RumorSpread preferencesRumorSpread = new PreferencesRumorSpread();
    private static RumorService rumorService;
    private RumorSpread centralityRumorSpread = new CentralityRumorSpread();
    private int num = 0;

    public static RumorService getInstance() {
        if(rumorService == null){
            rumorService = new RumorService();
        }
        return rumorService;
    }

    public void displayGraph(String fileName, int mode) {

        rumorService.setGraph(new SocialNetwork("Social Network", fileName, mode));
        this.graph.display();


        System.out.println("diametre : " + metrics.diametre(this.graph));
        System.out.println("distance moyenne : " + metrics.distance_moyenne(this.graph));
        System.out.println("densité moyenne : " + metrics.densite(this.graph));
        System.out.println("coefficient de clustering : " + metrics.clustering(this.graph));

        Iterator<? extends Node> iterator = this.graph.getEachNode().iterator();
        while (iterator.hasNext()) {
            Node next = iterator.next();
            int id = Integer.parseInt(next.getId().substring(1));
            next.setAttribute("centrality", metrics.centrality(this.graph, id));
        }
    }

    public void spread(Rumor rumor, String spreadStrategy) {

        if(spreadStrategy.equals(Constant.SPREAD_SIMPLE)) {
            RumorService.this.simpleRumorSpread.spread(rumor,RumorService.this.graph, this.num);
        } else if (spreadStrategy.equals(Constant.SPREAD_PREFERENCES)) {
            RumorService.this.preferencesRumorSpread.spread(rumor,RumorService.this.graph, this.num);
        } else if (spreadStrategy.equals(Constant.SPREAD_CENTRALITY)) {
            RumorService.this.centralityRumorSpread.spread(rumor,RumorService.this.graph, this.num);
        }

        this.num++;
    }

    public void setGraph(SocialNetwork graph) {
        this.graph = graph;
    }

    public void parameterGraph(int nbNodes, double lambda) {
        SocialNetwork.NB_NODES = nbNodes;
        SocialNetwork.COEF_DISTRIB = lambda;
    }
}
