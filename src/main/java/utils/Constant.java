/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package utils;

public class Constant {

    public static final String KEY_SPORT = "sport";
    public static final String KEY_CINEMA = "cinema";
    public static final String KEY_POLITIC = "politic";
    public static final String KEY_MUSIC = "music";
    public static final String SPREAD_SIMPLE = "Simple";
    public static final String SPREAD_PREFERENCES = "Preference";
    public static final String SPREAD_CENTRALITY = "Centrality";
    public static final String SPREAD_MSM = "msm";

    public static final int THRESHOLD_OF_INTEREST = 50;
    public static final double THRESHOLD_OF_TRUST_TO_BECOME_SPREADER = 0.4;
    public static final double THRESHOLD_OF_TRUST_TO_BECOME_STIFFLER = 0.2;

    public static final int MODE_SAVE = 0;
    public static final int MODE_RETRIEVE = 1;

}

