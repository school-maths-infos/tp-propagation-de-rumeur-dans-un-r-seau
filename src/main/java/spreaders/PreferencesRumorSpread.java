/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package spreaders;

import model.Rumor;
import model.Timeline;
import model.persons.PreferencesPerson;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.Iterator;
import java.util.LinkedList;

public class PreferencesRumorSpread extends RumorSpread {

    private static boolean defined = false;

    public class SpreadThread extends Thread {

        private final Rumor rumor;
        private Graph graph;
        private PreferencesRumorSpread preferencesRumorSpread;
        private int num;

        public SpreadThread(Graph graph, PreferencesRumorSpread preferencesRumorSpread, Rumor rumor, int num) {
            this.graph = graph;
            this.preferencesRumorSpread = preferencesRumorSpread;
            this.rumor = rumor;
            this.num = num;
        }

        public void run() {
            LinkedList<Node> file = new LinkedList<Node>();

            for (int i = 0; i < 3; i++) {
                Node source = graph.getNode(i);
                source.setAttribute("ui.class", "origin");
                PreferencesPerson personSource = source.getAttribute("person");
                personSource.horloge = 0;
                personSource.becomeSpreader(this.rumor);
                //Mettre le nœud source dans la file.
                file.add(source);
            }

            while (!file.isEmpty()) {
                //Retirer le nœud du début de la file pour l'examiner.
                Node currentNode = file.poll();
                PreferencesPerson currentPerson = currentNode.getAttribute("person");
                if (currentPerson.isSpreader(this.rumor)) {
                    currentNode.setAttribute("ui.class", "current" + this.num); //Elle est contaminée ET c'est elle qui est en train de diffuser à ses voisins

                    Iterator<? extends Node> iterator = currentNode.getNeighborNodeIterator();

                    while (iterator.hasNext()) {
                        // Get next nodes
                        Node next = iterator.next();
                        Edge edge = next.getEdgeBetween(currentNode);
                        Double trust = edge.getAttribute("trust");
                        PreferencesPerson personNext = next.getAttribute("person");

                        if (personNext.isInterested(this.rumor) && personNext.isIgnorant(this.rumor) && currentPerson.isSpreader(this.rumor)) {
                            flash(currentNode, next, this.num);
                            // Next nodes are exposed
                            personNext.setTrust(trust);
                            currentPerson.horloge += 1;
                            personNext.horloge = Math.max(personNext.horloge, currentPerson.horloge);
                            personNext.expose(this.rumor);
                            if (personNext.isSpreader(this.rumor)) {
                                next.setAttribute("ui.class", "spreader" + this.num);
                                file.add(next);
                            } else if (personNext.isIgnorant(this.rumor)) {
                                file.add(next);
                            } else {
                                next.setAttribute("ui.class", "stifler");
                            }
                        }
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    currentNode.setAttribute("ui.class", "spreader" + this.num);
                    //Si la file n'est pas vide reprendre à l'étape 2.
                }
            }

            Timeline.getInstance().display();
        }
    }

    @Override
    public void spread(Rumor rumor, Graph graph, int num) {

        if(!defined) {

            defined = true;

            //On peuple le graphe avec des personnes du type correspondant au diffuseur
            Iterator<? extends Node> nodeIterator = graph.getEachNode().iterator();

            while (nodeIterator.hasNext()) {
                Node node = nodeIterator.next();
                PreferencesPerson person = new PreferencesPerson(node.getDegree());
                node.setAttribute("person", person);
                node.addAttribute("ui.label", node.getId() + " " + person.getCentersOfInterestToString());
            }

            Iterator<? extends Edge> edgeIterator = graph.getEachEdge().iterator();

            while (edgeIterator.hasNext()) {
                Edge edge = edgeIterator.next();
                edge.addAttribute("trust", Math.random());
            }
        }

        //On effectue la diffusion
        SpreadThread spreadThread = new SpreadThread(graph, this, rumor, num);
        spreadThread.start();
    }

}
