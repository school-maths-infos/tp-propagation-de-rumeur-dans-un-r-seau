/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package spreaders;

import model.Rumor;
import model.persons.SimplePerson;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.Iterator;

//TODO (pas fait)

public class MsmRumorSpread extends RumorSpread {

    //L'utilisation d'un thread est nécessaire,
    // car si la diffusion se passe sur le même thread que l'affichage du graphe,
    // alors l'affichage du graphe ne se met à jour qu'après la fin de la diffusion => on ne voit que le résultat
    public class SpreadThread extends Thread {

        private Graph graph;
        private Rumor rumor;
        private int num;

        public SpreadThread(Graph graph, Rumor rumor, int num) {
            this.graph = graph;
            this.rumor = rumor;
            this.num = num;
        }
    }

    @Override
    public void spread(Rumor rumor, Graph graph, int num) {

        Iterator<? extends Node> nodeIterator = graph.getEachNode().iterator();

        while (nodeIterator.hasNext()) {
            Node node = nodeIterator.next();
            node.setAttribute("person", new SimplePerson(node.getDegree()));
            node.setAttribute("ui.label", node.getId());
        }

        SpreadThread spreadThread = new SpreadThread(graph, rumor, num);
        spreadThread.start();

    }
}
