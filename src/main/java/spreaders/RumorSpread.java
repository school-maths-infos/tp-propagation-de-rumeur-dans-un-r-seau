/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package spreaders;

import model.Rumor;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

public abstract class RumorSpread {

    //x = la capacité de la personne = nb de voisins qu'elle veut contaminer
    //Simple : donne à x voisins choisis aléatoirement parmis tous => Céline
    //Par intérêt : aux x voisins les plus susceptibles de s'intéresser => Juliana
    //Par plus haut degré => Céline
    //Par degré d'intermédiarité => Juliana
    //Par degré de proximité => Céline

    //Par SIR

    public abstract void spread(Rumor rumor, Graph graph, int num);

    protected void flash(Node currentNode, Node next, int num) {
        currentNode.getEdgeBetween(next).setAttribute("ui.class", "circule" + num);
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        currentNode.getEdgeBetween(next).setAttribute("ui.class", "noir");
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        currentNode.getEdgeBetween(next).setAttribute("ui.class", "circule" + num);
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        currentNode.getEdgeBetween(next).setAttribute("ui.class", "noir");
    }
}
