/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package spreaders;

import model.Rumor;
import model.persons.SimplePerson;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import model.Timeline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class SimpleRumorSpread extends RumorSpread {

    private static boolean defined = false;

    //L'utilisation d'un thread est nécessaire,
    // car si la diffusion se passe sur le même thread que l'affichage du graphe,
    // alors l'affichage du graphe ne se met à jour qu'après la fin de la diffusion => on ne voit que le résultat
    public class SpreadThread extends Thread {

        private Graph graph;
        private Rumor rumor;
        private int num;

        public SpreadThread(Graph graph, Rumor rumor, int num) {
            this.graph = graph;
            this.rumor = rumor;
            this.num = num;
        }

        public void run() {

            Node source = graph.getNode((int)Math.floor(Math.random()*graph.getNodeCount()));
            SimplePerson personSource = (SimplePerson) source.getAttribute("person");
            personSource.horloge = 0;
            personSource.becomeSpreader(rumor);
            source.setAttribute("ui.class", "origin");
            System.out.println(source.getId() + " est à l'origine de la rumeur");

            LinkedList<Node> file = new LinkedList<Node>();

            //Mettre le nœud source dans la file.
            file.add(source);

            while (!file.isEmpty()) {
                //Retirer le nœud du début de la file pour l'examiner.
                Node currentNode = file.poll();
                SimplePerson currentPerson = (SimplePerson) currentNode.getAttribute("person");
                if(currentPerson.isSpreader(this.rumor)) {
                    currentNode.setAttribute("ui.class", "current" + this.num);

                    //Mettre tous les voisins non explorés dans la file (à la fin).
                    Iterator<? extends Node> iterator = currentNode.getNeighborNodeIterator();
                    ArrayList<Node> neighbours = new ArrayList<Node>();
                    while (iterator.hasNext()) {
                        Node next = iterator.next();
                        SimplePerson person = (SimplePerson) next.getAttribute("person");
                        if (person.isIgnorant(this.rumor)) {
                            neighbours.add(next);
                        }
                    }
                    if (neighbours.size() > 0) {
                        Node chosenNeighbour = neighbours.get((int) Math.floor(Math.random() * neighbours.size()));
                        flash(currentNode, chosenNeighbour, this.num);

                        SimplePerson person = (SimplePerson) chosenNeighbour.getAttribute("person");
                        currentPerson.horloge += 1;
                        person.horloge = Math.max(person.horloge, currentPerson.horloge);
                        person.expose(this.rumor);

                        if (person.isSpreader(this.rumor)) {
                            file.add(chosenNeighbour);
                            chosenNeighbour.setAttribute("ui.class", "current" + this.num);
                        }
                    }
                    if (neighbours.size() > 1) {
                        file.add(currentNode);
                    }
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    currentNode.setAttribute("ui.class", "spreader" + this.num);
                }
                //Si la file n'est pas vide reprendre à l'étape 2.
            }

            Timeline.getInstance().display();
            System.out.println("Fin de la propagation de la rumeur");
        }

    }

    public void spread(Rumor rumor, Graph graph, int num) {

        if(!defined) {

            defined = true;

            Iterator<? extends Node> nodeIterator = graph.getEachNode().iterator();

            while (nodeIterator.hasNext()) {
                Node node = nodeIterator.next();
                node.setAttribute("person", new SimplePerson(node.getDegree()));
                node.setAttribute("ui.label", node.getId());
            }
        }

        SpreadThread spreadThread = new SpreadThread(graph,rumor, num);
        spreadThread.start();

    }
}
