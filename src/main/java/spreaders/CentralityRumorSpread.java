/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package spreaders;

import model.Rumor;
import model.Timeline;
import model.persons.SimplePerson;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.Iterator;
import java.util.LinkedList;

public class CentralityRumorSpread extends RumorSpread {


    //L'utilisation d'un thread est nécessaire,
    // car si la diffusion se passe sur le même thread que l'affichage du graphe,
    // alors l'affichage du graphe ne se met à jour qu'après la fin de la diffusion => on ne voit que le résultat
    public class SpreadThread extends Thread {

        private Graph graph;
        private Rumor rumor;
        private int num;

        public SpreadThread(Graph graph, Rumor rumor, int num) {
            this.graph = graph;
            this.rumor = rumor;
            this.num = num;
        }

        public void run() {

            Node source = graph.getNode((int)Math.floor(Math.random()*graph.getNodeCount()));
            SimplePerson personSource = (SimplePerson) source.getAttribute("person");
            personSource.horloge = 0;
            personSource.becomeSpreader(rumor);
            source.setAttribute("ui.class", "origin");
            System.out.println(source.getId() + " est à l'origine de la rumeur");

            LinkedList<Node> file = new LinkedList<Node>();

            //Mettre le nœud source dans la file.
            file.add(source);

            while (!file.isEmpty()) {
                //Retirer le nœud du début de la file pour l'examiner.
                Node currentNode = file.poll();
                SimplePerson currentPerson = (SimplePerson) currentNode.getAttribute("person");
                if(currentPerson.isSpreader(this.rumor)) {

                    currentNode.setAttribute("ui.class", "current" + this.num);
                    //Choisir le voisin non exploré le plus central et le mettre dans la file (à la fin).
                    double centrality = (double) Integer.MAX_VALUE;
                    Node chosenNeighbour = null;
                    Iterator<? extends Node> iterator = currentNode.getNeighborNodeIterator();
                    while (iterator.hasNext()) {
                        Node next = iterator.next();
                        SimplePerson person = (SimplePerson) next.getAttribute("person");
                        if (person.isIgnorant(this.rumor)) {
                            if ((Double) next.getAttribute("centrality") < centrality) {
                                centrality = (Double) next.getAttribute("centrality");
                                chosenNeighbour = next;
                            }
                        }

                    }

                    if (chosenNeighbour != null) {
                        SimplePerson person = (SimplePerson) chosenNeighbour.getAttribute("person");
                        flash(currentNode, chosenNeighbour, this.num);

                        currentPerson.horloge += 1;
                        person.horloge = Math.max(person.horloge, currentPerson.horloge);
                        person.expose(this.rumor);

                        if (person.isSpreader(this.rumor)) {
                            file.add(chosenNeighbour);
                            chosenNeighbour.setAttribute("ui.class", "current" + this.num);
                        }
                        file.add(currentNode);
                    }
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SimplePerson person = (SimplePerson) currentNode.getAttribute("person");
                    if (person.isSpreader(this.rumor)) {
                        currentNode.setAttribute("ui.class", "spreader" + this.num);
                    }
                    //Si la file n'est pas vide reprendre à l'étape 2.
                }
            }

            Timeline.getInstance().display();
            System.out.println("Fin de la propagation de la rumeur");
        }

    }

    public void spread(Rumor rumor, Graph graph, int num) {

        Iterator<? extends Node> nodeIterator = graph.getEachNode().iterator();

        while (nodeIterator.hasNext()) {
            Node node = nodeIterator.next();
            node.setAttribute("person", new SimplePerson(node.getDegree()));
            node.setAttribute("ui.label", node.getId());
        }

        SpreadThread spreadThread = new SpreadThread(graph, rumor, num);
        spreadThread.start();

    }
}
