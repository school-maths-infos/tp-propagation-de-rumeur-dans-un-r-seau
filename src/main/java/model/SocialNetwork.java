/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model;

import org.graphstream.graph.Edge;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceFactory;
import utils.Constant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class SocialNetwork extends SingleGraph {

    public static double COEF_DISTRIB = 1.4;
    public static int NB_NODES = 50;

    private ArrayList<SemiLink> semiLinks;
    private ArrayList<SemiLink> links;

    private int[][] adjacence = new int[NB_NODES][NB_NODES];

    protected final static String styleSheet =
            "node.origin {" +
                    "       fill-color: green;" +
                    "}" +
                    "node.stifler {" +
                    "       fill-color: gray;" +
                    "}" +
                    "node.spreader0 {" +
                    "       fill-color: purple;" +
                    "}" +
                    "node.spreader1 {" +
                    "       fill-color: red;" +
                    "}" +
                    "node.spreader2 {" +
                    "       fill-color: orange;" +
                    "}" +
                    "node.spreader3 {" +
                    "       fill-color: cyan;" +
                    "}" +
                    "node.spreader4 {" +
                    "       fill-color: magenta;" +
                    "}" +
                    "node.current0 {" +
                    "       size: 20px;" +
                    "       fill-color: purple;" +
                    "}" +
                    "node.current1 {" +
                    "       size: 20px;" +
                    "       fill-color: red;" +
                    "}" +
                    "node.current2 {" +
                    "       size: 20px;" +
                    "       fill-color: orange;" +
                    "}" +
                    "node.current3 {" +
                    "       size: 20px;" +
                    "       fill-color: cyan;" +
                    "}" +
                    "node.current4 {" +
                    "       size: 20px;" +
                    "       fill-color: magenta;" +
                    "}" +
                    "edge.circule0 {" +
                    "        fill-color: purple;" +
                    "        size: 5px;" +
                    "}" +
                    "edge.circule1 {" +
                    "        fill-color: red;" +
                    "        size: 5px;" +
                    "}" +
                    "edge.circule2 {" +
                    "        fill-color: orange;" +
                    "        size: 5px;" +
                    "}" +
                    "edge.circule3 {" +
                    "        fill-color: cyan;" +
                    "        size: 5px;" +
                    "}" +
                    "edge.circule4 {" +
                    "        fill-color: magenta;" +
                    "        size: 5px;" +
                    "}" +
                    "edge.noir {" +
                    "       fill-color: black;" +
                    "       size: 1px;" +
                    "}";


    public SocialNetwork(String name, String fileName, int mode) {

        super(name);

        for (int i = 0; i < NB_NODES; i++) {
            for (int j = 0; j < NB_NODES; j++) {
                this.adjacence[i][j] = 0;
            }
        }

        if (mode == Constant.MODE_RETRIEVE) {

            try {
                FileSource fs = FileSourceFactory.sourceFor(fileName);
                fs.addSink(this);
                fs.readAll(fileName);

                Iterator<? extends Edge> iterator = this.getEdgeIterator();
                while (iterator.hasNext()) {
                    Edge next = iterator.next();
                    int idSrcNode = Integer.parseInt(next.getNode0().getId().substring(1));
                    int idDestNode = Integer.parseInt(next.getNode1().getId().substring(1));
                    this.adjacence[idSrcNode][idDestNode] = 1;
                    this.adjacence[idDestNode][idSrcNode] = 1;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (mode == Constant.MODE_SAVE) {

            this.genSuite();
            while (!verifyDegreesSum()) {
                this.genSuite();
            }

            this.createNodes();
            this.connect();

            this.addAttribute("ui.stylesheet", styleSheet);

            try {
                this.write(fileName);
                this.write(fileName.replace(".dgs", ".gexf"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private double invDistrib(double nb) {
        return Math.floor(Math.pow(nb, -1 / COEF_DISTRIB));
    }

    private void createNodes() {

        for (int i = 0; i < NB_NODES; i++) {
            this.addNode("n" + i);
        }
    }

    private void connect() {

        System.out.println("SocialNetwork.connect");
        while (!semiLinks.isEmpty()) {
            SemiLink slSrc = semiLinks.get((int) (Math.floor(Math.random() * semiLinks.size())));
            if (verifyFreeConnection(slSrc)) {
                SemiLink slDest = semiLinks.get((int) (Math.floor(Math.random() * semiLinks.size())));
                boolean linkExistYet = linkExistYet(slSrc, slDest);
                while (slSrc.idNode == slDest.idNode || linkExistYet) {
                    slDest = semiLinks.get((int) (Math.floor(Math.random() * semiLinks.size())));
                    linkExistYet = linkExistYet(slSrc, slDest);
                }

                slSrc.connectedWith = slDest;
                semiLinks.remove(slSrc);
                semiLinks.remove(slDest);
                links.add(slSrc);
                System.out.print(".");
            } else {
                if (!removeALinkFor(slSrc)) {
                    semiLinks.remove(slSrc);
                    System.err.println("Probleme : je ne peux pas connecter " + slSrc.idNode);
                }
            }
        }
        System.out.println();

        for (SemiLink link : links) {
            int idSrcNode = link.idNode;
            int idDestNode = link.connectedWith.idNode;
            String idEdge;
            if (idSrcNode < idDestNode) {
                idEdge = idSrcNode + "-" + idDestNode;
            } else {
                idEdge = idDestNode + "-" + idSrcNode;
            }
            this.addEdge(idEdge, idSrcNode, idDestNode);
            this.adjacence[idSrcNode][idDestNode] = 1;
            this.adjacence[idDestNode][idSrcNode] = 1;
        }

    }

    private boolean linkExistYet(SemiLink slSrc, SemiLink slDest) {
        boolean linkExistYet = false;
        for (SemiLink link : links) {
            if (
                    (link.idNode == slSrc.idNode && link.connectedWith.idNode == slDest.idNode)
                            || (link.idNode == slDest.idNode && link.connectedWith.idNode == slSrc.idNode)
                    ) {
                linkExistYet = true;
            }
        }
        return linkExistYet;
    }

    private boolean removeALinkFor(SemiLink slSrc) {

        for (SemiLink link : links) {
            if (link.idNode != slSrc.idNode || link.connectedWith.idNode != slSrc.idNode) {
                links.remove(link);
                semiLinks.add(new SemiLink(link.idNode));
                semiLinks.add(new SemiLink(link.connectedWith.idNode));
                return true;
            }
        }
        return false;
    }

    private boolean verifyFreeConnection(SemiLink slSrc) {
        for (SemiLink slDest : semiLinks) {
            if (slDest.idNode != slSrc.idNode) {
                boolean linkExistYet = linkExistYet(slSrc, slDest);
                if (!linkExistYet) return true;
            }
        }
        return false;
    }

    private void genSuite() {
        semiLinks = new ArrayList<SemiLink>();
        links = new ArrayList<SemiLink>();

        for (int i = 0; i < NB_NODES; i++) {
            double k = invDistrib(Math.random());
            if (k > NB_NODES / 2.0) {
                k = NB_NODES / 2.0;
            }
            for (int j = 0; j < (int) k; j++) {
                semiLinks.add(new SemiLink(i));
            }
        }
    }

    private boolean verifyDegreesSum() {

        return (semiLinks.size() % 2 == 0);
    }

    public int[][] getAdjacence() {
        return adjacence;
    }
}
