/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model;

public class SemiLink {

    public int idNode;
    public SemiLink connectedWith = null;

    SemiLink(int id) {
        idNode = id;
    }
}
