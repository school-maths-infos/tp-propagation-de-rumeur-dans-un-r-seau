/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model;

import java.util.HashMap;

public class Rumor {

    private int levelOfInterest;
    private HashMap<String,Integer> centersOfInterest;
    private String content;

    //Msm
    public int vraisemblable;
    //Info souhaitable = levelOfInterest


    public Rumor(String content, int levelOfInterest, HashMap centersOfInterest) {
        this.content = content;
        this.levelOfInterest = levelOfInterest;
        this.centersOfInterest = centersOfInterest;
    }

    public int getLevelOfInterest() {
        return levelOfInterest;
    }

    public HashMap<String, Integer> getCentersOfInterest() {
        return centersOfInterest;
    }

    public String getContent() {
        return content;
    }
}
