/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model;

import org.graphstream.algorithm.measure.ChartMeasure;
import org.graphstream.algorithm.measure.ChartSeries1DMeasure;

import java.util.*;

public class Timeline {

    private static Timeline instance;
    private Map<Rumor, Map<Integer, Integer>> nbSpreadersByRumorAndTime;
    private int finished = 0;

    private Timeline() {

        this.nbSpreadersByRumorAndTime = new HashMap<Rumor, Map<Integer, Integer>>();
    }

    public static Timeline getInstance() {

        if (instance == null) {
            instance = new Timeline();
        }

        return instance;
    }

    public void addSpreader(int time, Rumor rumor) {

        Map<Integer, Integer> nbSpreadersByTime;
        if(this.nbSpreadersByRumorAndTime.containsKey(rumor)) {
            nbSpreadersByTime = this.nbSpreadersByRumorAndTime.get(rumor);
        } else {
            nbSpreadersByTime = new HashMap<Integer, Integer>();
            this.nbSpreadersByRumorAndTime.put(rumor, nbSpreadersByTime);
        }
        if (nbSpreadersByTime.containsKey(time)) {
            int nbSpreaders = nbSpreadersByTime.get(time) + 1;
            nbSpreadersByTime.put(time, nbSpreaders);
        } else {
            nbSpreadersByTime.put(time, 1);
        }

    }

    public void remSpreader(int time, Rumor rumor) {

        Map<Integer, Integer> nbSpreadersByTime;
        if(this.nbSpreadersByRumorAndTime.containsKey(rumor)) {
            nbSpreadersByTime = this.nbSpreadersByRumorAndTime.get(rumor);
        } else {
            nbSpreadersByTime = new HashMap<Integer, Integer>();
            this.nbSpreadersByRumorAndTime.put(rumor, nbSpreadersByTime);
        }
        if (nbSpreadersByTime.containsKey(time)) {
            int nbSpreaders = nbSpreadersByTime.get(time) - 1;
            nbSpreadersByTime.put(time, nbSpreaders);
        } else {
            nbSpreadersByTime.put(time, -1);
        }

    }

    public void display() {

        finished++;
        if(finished == this.nbSpreadersByRumorAndTime.keySet().size()) {
            ChartMeasure.PlotParameters plotParameters = new ChartMeasure.PlotParameters();
            plotParameters.xAxisLabel = "temps";
            plotParameters.yAxisLabel = "nombre de spreaders";

            for (Rumor rumor : this.nbSpreadersByRumorAndTime.keySet()) {
                Map<Integer, Integer> nbSpreadersByTime = this.nbSpreadersByRumorAndTime.get(rumor);

                ChartSeries1DMeasure chartNbSpreader = new ChartSeries1DMeasure("Rumeur " + rumor.getContent());

                int valeurCumulee = 0;

                int maximum = 0;
                for (Integer i : nbSpreadersByTime.keySet()) {
                    if (i > maximum) {
                        maximum = i;
                    }
                }

                for (int i = 0; i < maximum; i++) {
                    if (nbSpreadersByTime.get(i) != null) {
                        valeurCumulee += nbSpreadersByTime.get(i);
                    }
                    chartNbSpreader.addValue(valeurCumulee);
                }

                try {
                    chartNbSpreader.plot(plotParameters);
                } catch (ChartMeasure.PlotException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
