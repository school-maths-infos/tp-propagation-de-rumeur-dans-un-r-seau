/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model;


import org.graphstream.graph.Node;

import java.io.IOException;
import java.util.Iterator;

public class Metrics {

    private int[][] warshall;
    private int diametre;

    public void displayMatrix(int[][] matrix, int nbNodes) {

        for (int i = 0; i < nbNodes; i++) {
            for (int j = 0; j < nbNodes; j++) {
                if (matrix[i][j] < 10) {
                    System.out.print(" " + matrix[i][j] + " | ");
                } else if (matrix[i][j] == Integer.MAX_VALUE) {
                    System.out.print("-- | ");
                } else {
                    System.out.print(matrix[i][j] + " | ");
                }
            }
            System.out.println();
        }
    }

    public int diametre(SocialNetwork graph) {

        int nbNodes = graph.getNodeCount();
        int[][] adjacence = graph.getAdjacence();
        warshall = new int[nbNodes][nbNodes];

        System.out.println("Metrics.diametre generating warshall");
        for (int i = 0; i < nbNodes; i++) {
            for (int j = 0; j < nbNodes; j++) {
                if (i==j) {
                    warshall[i][j] = 0;
                } else if (adjacence[i][j] == 1) {
                    warshall[i][j] = 1;
                } else {
                    warshall[i][j] = Integer.MAX_VALUE;
                }
            }
        }

        for (int k = 0; k < nbNodes; k++) {
            for (int i = 0; i < nbNodes; i++) {
                for (int j = 0; j < nbNodes; j++) {
                    if (warshall[i][j] == Integer.MAX_VALUE) {
                        if (warshall[i][k] != Integer.MAX_VALUE && warshall[k][j] != Integer.MAX_VALUE) {
                            warshall[i][j] = warshall[i][k] + warshall[k][j];
                        }
                    } else if (warshall[i][k] != Integer.MAX_VALUE && warshall[k][j] != Integer.MAX_VALUE) {
                        warshall[i][j] = Math.min(warshall[i][j],warshall[i][k] + warshall[k][j]);
                    }
                }
            }

            System.out.print(".");
        }
        System.out.println();

        System.out.println("Metrics.diametre");
        diametre = 0;

        for (int i = 0; i < nbNodes; i++) {
            for (int j = 0; j < nbNodes; j++) {
                if (warshall[i][j] > diametre && warshall[i][j] != Integer.MAX_VALUE) {
                    diametre = warshall[i][j];
                }
            }
            System.out.print(".");
        }
        System.out.println();

        /*System.out.println("Adjacence");
        this.displayMatrix(adjacence,nbNodes);
        System.out.println("Warshall");;
        this.displayMatrix(warshall,nbNodes);*/

        return diametre;
    }

    public int distance(int idSource, int idDest) {
        return warshall[idSource][idDest];
    }

    public double distance_moyenne(SocialNetwork graph) {

        System.out.println("Metrics.distance_moyenne");
        int nbNodes = graph.getNodeCount();
        int s = 0;
        int nbPaires = 0;

        for (int i = 0; i < nbNodes; i++) {
            for (int j = 0; j < nbNodes; j++) {
                if (warshall[i][j] != Integer.MAX_VALUE && i != j) {
                    s += warshall[i][j];
                    nbPaires++;
                }
            }
            System.out.print(".");
        }
        System.out.println();

        return (double)s/(double)nbPaires ;
    }

    public double densite(SocialNetwork graph) {

        int nbEdges = graph.getEdgeCount();
        int nbNodes = graph.getNodeCount();
        return (double) nbEdges / (double) (nbNodes*(nbNodes - 1) / 2);
    }

    //On calcule la moyenne des distances géodésiques du noeud donné vers les autres noeuds du graphe
    //Plus c'est petit, plus le noeud est central
    public double centrality(SocialNetwork graph, int idSource) {

        int s = 0;
        int nbNodes = graph.getNodeCount();

        for (int j = 0; j < nbNodes; j++) {
            if(warshall[idSource][j] != Integer.MAX_VALUE) {
                s += warshall[idSource][j];
            } else {
                s += diametre + 1;
            }
        }

        return (double) s / (double) nbNodes;
    }

    //On calule le coefficient de clustering moyen
    // Qui correspond au rapport de la somme des coefficients locaux et du nombre de liens
    //
    public double clustering(SocialNetwork graph) {
        System.out.println("Metrics.clustering");

        double sumLocalClustering = 0;

        graph.getEachEdge();
        Iterator<? extends Node> iterator = graph.getNodeIterator();
        int k = graph.getNodeCount();
        while (iterator.hasNext()) {
            double localClustering = 0;
            //Calcul du coefficient de clustering local
            Node currentNode = iterator.next();
            double degree = currentNode.getDegree();
            double nodesSet = 0;
            //On parcourt les voisins et regarde s'il existe des triangles
            //On prend les voisins de currentNode
            Iterator<? extends Node> neighborNodeIterator = currentNode.getNeighborNodeIterator();
            while (neighborNodeIterator.hasNext()) {
                nodesSet = 0;
                Node neighborCurrentNode = neighborNodeIterator.next();
                //Pour chaque voisin du voisin de currentNode
                //On regarde si un lien existe avec currentNode
                Iterator<? extends Node> neighborOfNeighborNodeIterator = neighborCurrentNode.getNeighborNodeIterator();
                while (neighborOfNeighborNodeIterator.hasNext()) {
                    Node neighborOfNeighborNode = neighborOfNeighborNodeIterator.next();
                    if (!neighborOfNeighborNode.equals(currentNode)) {
                        if (neighborOfNeighborNode.hasEdgeBetween(currentNode)) {
                            nodesSet++;
                        }
                    }
                }
            }
            //System.out.println("Noeud : " + currentNode.toString() + " - " + "Nombre de triangles : " + nodesSet + " - Degré: " + degree);
            if (degree > 1) {
                localClustering = 2 * nodesSet / (degree * (degree - 1));
            }
            sumLocalClustering += localClustering;

            System.out.print(".");
        }
        System.out.println();
        return sumLocalClustering / graph.getEdgeCount();
    }

}
