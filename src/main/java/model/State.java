/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model;

public class State {

    private boolean spreader = false;
    private boolean stifler = false;
    private boolean ignorant = true;
    public double repetition = 0;

    public boolean isSpreader() {
        return this.spreader;
    }

    public void becomeSpreader() {
        this.spreader = true;
        this.ignorant = false;
        this.stifler = false;
    }

    public void becomeStifler() {
        this.spreader = false;
        this.stifler = true;
        this.ignorant = false;
    }

    public void becomeIgnorant() {
        this.stifler = false;
        this.spreader = false;
        this.ignorant = true;
    }

    public boolean isStifler() {
        return this.stifler;
    }

    public boolean isIgnorant() {
        return this.ignorant;
    }
}
