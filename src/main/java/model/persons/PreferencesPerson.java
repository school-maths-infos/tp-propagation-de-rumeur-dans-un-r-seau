/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model.persons;

import model.Rumor;
import model.State;
import model.Timeline;
import utils.Constant;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PreferencesPerson extends Person {

    private double activity;
    private HashMap<String,Integer> centersOfInterest;

    private Double trust;

    public PreferencesPerson(int degree) {
        super(degree);
        this.activity = Math.random() * degree;

        centersOfInterest = new HashMap<String, Integer>();
        centersOfInterest.put(Constant.KEY_SPORT,(int) ((Math.random() * this.activity) % 1 * 100));
        centersOfInterest.put(Constant.KEY_POLITIC,(int) ((Math.random() * this.activity) % 1 * 100));
        centersOfInterest.put(Constant.KEY_CINEMA,(int) ((Math.random() * this.activity) % 1 * 100));
        centersOfInterest.put(Constant.KEY_MUSIC,(int) ((Math.random() * this.activity) % 1 * 100));

        this.rumors = new HashMap<Rumor, State>();

    }

    @Override
    public void expose(Rumor rumor) {
        if (!this.rumors.containsKey(rumor)) {
            this.rumors.put(rumor, new State());
        }

        State state = this.rumors.get(rumor);
        if (state.isIgnorant()) {
            state.repetition += this.trust;
            if (this.trust <= Constant.THRESHOLD_OF_TRUST_TO_BECOME_STIFFLER) {
                state.becomeStifler();
            } else if (state.repetition >= Constant.THRESHOLD_OF_TRUST_TO_BECOME_SPREADER) {
                if (this.rumors.keySet().size() == 1) {
                    this.becomeSpreader(rumor);
                }
                becomeSpreaderIfBetterThanOtherRumors(rumor, state);
            }
        }
    }

    public String getCentersOfInterestToString() {
        String interests = "";
        for(String key : this.centersOfInterest.keySet()) {
            interests += key.substring(0, 1) + " : " + this.centersOfInterest.get(key) + " ";
        }

        return interests;
    }

    public boolean isInterested(Rumor rumor) {
        boolean isInterested = false;
        Iterator<Map.Entry<String, Integer>> entries = this.centersOfInterest.entrySet().iterator();
        while (!isInterested && entries.hasNext()) {
            Map.Entry entry = entries.next();
            if (rumor.getCentersOfInterest().containsKey(entry.getKey())) {
                int interest = rumor.getCentersOfInterest().get(entry.getKey());
                isInterested = (interest == 1 && this.centersOfInterest.get(entry.getKey()) * rumor.getLevelOfInterest()
                        >= Constant.THRESHOLD_OF_INTEREST);
            }
        }
        return isInterested;
    }

    public void setTrust(Double trust) {
        this.trust = trust;
    }
}
