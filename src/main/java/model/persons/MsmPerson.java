/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model.persons;

import model.Rumor;
import model.State;

public class MsmPerson extends Person {

    public int source_credible;
    public int credulite;
    //Repetition dans l'etat
    public int role;
    /*
    « L’instigateur » : celui qui pose la question ou qui amène un doute
    - « L’interprète » : celui qui répond aux interrogations de l’instigateur et propose une
    explication cohérente et convaincante
    - « Le leader d’opinion » : celui dont l’avis va déterminer l’opinion du groupe.
    - « Les apôtres » : qui, s’identifiant totalement à la rumeur, tentent de convaincre la cité.
    - « Le récupérateur » : qui trouve un intérêt à ce que la rumeur se poursuive, sans
    nécessairement la croire.
    - « L’opportuniste » qui s’en sert pour affermir son autorité morale
    - « le flirteur » ne croit pas la rumeur mais la savoure avec délice. Il joue en parlant d’elle
    autour de lui, prenant plaisir à créer un certain trouble dans son auditoire.
    - « Les relais passifs » : ceux qui se déclarent ne pas être convaincus par la rumeur mais qui
    questionnent l’entourage, soupçonneux de connaître la vérité.
    - « Les résistants » : mènent la riposte et constituent des protagonistes à l’anti-rumeur. => stifer */
        public static final int ROLE_INSTIGATEUR = 0;
        public static final int ROLE_INTERPRETE = 1;
        public static final int ROLE_LEADER = 2;
        public static final int ROLE_APOTRE = 3;
        public static final int ROLE_RECUPERATEUR = 4;
        public static final int ROLE_PASSIF = 5;
        public static final int ROLE_FLIRTEUR = 6;


    public MsmPerson(int degree) {
        super(degree);
    }

    @Override
    public void expose(Rumor rumor) {

        ////////////////////////////////////////////////////////////////
        // TODO : j'ai copié simplePerson en attendant de m'en occuper

        if (!this.rumors.containsKey(rumor)) {
            this.rumors.put(rumor, new State());
        }
        State state = this.rumors.get(rumor);
        state.repetition++;

        if(state.isIgnorant()) {
            if (this.rumors.keySet().size() == 1) {
                this.becomeSpreader(rumor);
            }
            this.becomeSpreaderIfBetterThanOtherRumors(rumor,state);
        }

        ////////////////////////////////////////////////////////////////
    }
}
