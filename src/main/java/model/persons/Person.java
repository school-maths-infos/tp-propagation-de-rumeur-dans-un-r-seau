/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model.persons;

import model.Rumor;
import model.State;
import model.Timeline;

import java.util.HashMap;


public abstract class Person {

    protected int degree;

    protected HashMap<Rumor, State> rumors; //Etat = contaminé ou pas

    public int horloge = 0;

    public Person(int degree) {
        this.degree = degree;
        this.rumors = new HashMap<Rumor, State>();
    }

    protected void becomeSpreaderIfBetterThanOtherRumors(Rumor rumor, State state) {
        for(Rumor otherRumor: this.rumors.keySet()) {
            if (!otherRumor.equals(rumor)) {
                State s = this.rumors.get(otherRumor);
                //rumor est + recente (elle vient d'arriver)
                //si au moins un des deux autres critères est mieux pour rumor, on choisit rumor
                if (state.repetition >= s.repetition || rumor.getLevelOfInterest() >= otherRumor.getLevelOfInterest()) {
                    this.becomeSpreader(rumor);
                }
                //sinon on reste avec la rumeur ancienne
            }
        }
    }

    public void becomeSpreader(Rumor rumor) {

        Timeline.getInstance().addSpreader(this.horloge, rumor);
        for(Rumor precedentlySpreadingRumor: this.rumors.keySet()) {
            if(!precedentlySpreadingRumor.equals(rumor)) {
                State state = this.rumors.get(precedentlySpreadingRumor);
                if (state.isSpreader()) {
                    Timeline.getInstance().remSpreader(this.horloge, precedentlySpreadingRumor);
                    state.becomeStifler();
                }
            }
        }

        if (this.rumors.containsKey(rumor)) {
            this.rumors.get(rumor).becomeSpreader();
        } else {
            State state = new State();
            state.becomeSpreader();
            this.rumors.put(rumor, state);
        }
    }

    //La personne est exposée dès l'instant qu'on lui a communiqué la rumeur
    public abstract void expose(Rumor rumor);

    //La personne est spreader dès l'instant qu'elle souhaite communiquer la rumeur
    public boolean isSpreader(Rumor rumor) {

        boolean isSpreader = false; //Car par défaut la personne est ignorante (pas spreader)
        if (this.rumors.containsKey(rumor)) {
            isSpreader = this.rumors.get(rumor).isSpreader();
        }
        return isSpreader;
    }

    //La personne est ignorant tant qu'elle n'est pas spreader ou stifler
    public boolean isIgnorant(Rumor rumor) {

        boolean isIgnorant = true; //Car par défaut la personne est ignorante
        if (this.rumors.containsKey(rumor)) {
            isIgnorant = this.rumors.get(rumor).isIgnorant();
        }
        return isIgnorant;
    }

}
