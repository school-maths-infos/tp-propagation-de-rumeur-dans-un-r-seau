/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package model.persons;

import model.Rumor;
import model.State;

public class SimplePerson extends Person {


    public SimplePerson(int degree) {
        super(degree);
    }

    @Override
    public void expose(Rumor rumor) {
        if (!this.rumors.containsKey(rumor)) {
            this.rumors.put(rumor, new State());
        }
        State state = this.rumors.get(rumor);
        state.repetition++;

        if(state.isIgnorant()) {
            if (this.rumors.keySet().size() == 1) {
                this.becomeSpreader(rumor);
            }
            this.becomeSpreaderIfBetterThanOtherRumors(rumor,state);
        }
    }
}
