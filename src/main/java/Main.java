/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

import service.RumorService;
import view.RumorJFrame;

public class Main {

    public static void main(String args[]) {

        RumorJFrame.getInstance();
        RumorService rumorService = RumorService.getInstance();

    }
}
