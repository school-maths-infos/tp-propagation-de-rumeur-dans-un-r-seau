/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import javax.swing.*;
import java.awt.*;

public class PreferencePanel extends JPanel {

    public PreferencePanel() {

        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorPreferencePanel(new Color(128,0,128), "Preferences Violet"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorPreferencePanel(Color.RED, "Preferences Rouge"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorPreferencePanel(Color.ORANGE, "Preferences Orange"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorPreferencePanel(Color.CYAN, "Preferences Bleu"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorPreferencePanel(Color.MAGENTA, "Preferences Magenta"));
        this.add(Box.createHorizontalGlue());
    }
}
