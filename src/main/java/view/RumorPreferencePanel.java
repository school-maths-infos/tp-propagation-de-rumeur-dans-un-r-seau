/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import model.Rumor;
import service.RumorService;
import utils.Constant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class RumorPreferencePanel extends JPanel {

    public JSpinner jspinLevelOfInterest;
    public JSpinner.NumberEditor spinnerEditor;
    public String name;
    public Checkbox musicCheck;
    public Checkbox cineCheck;
    public Checkbox politiqueCheck;
    public Checkbox sportCheck;

    public RumorPreferencePanel(Color color, String name) {

        this.name = name;

        this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        JPanel top = new JPanel();
        top.setBackground(color);
        top.add(new JLabel("..."));
        this.add(top);

        this.add(Box.createRigidArea(new Dimension(10,10)));

        JPanel degree = new JPanel();
        degree.setLayout(new BoxLayout(degree,BoxLayout.LINE_AXIS));
        degree.add(new JLabel("Degré d'importance : "));
        jspinLevelOfInterest = new JSpinner(new SpinnerNumberModel(0,0,5,1));
        spinnerEditor = new JSpinner.NumberEditor(jspinLevelOfInterest);
        jspinLevelOfInterest.setEditor(spinnerEditor);
        degree.add(jspinLevelOfInterest);
        this.add(degree);

        this.add(Box.createRigidArea(new Dimension(10,10)));

        musicCheck = new Checkbox("Musique", true);
        cineCheck = new Checkbox("Cinéma", true);
        politiqueCheck = new Checkbox("Politique", true);
        sportCheck = new Checkbox("Sport", true);
        this.add(musicCheck);
        this.add(cineCheck);
        this.add(politiqueCheck);
        this.add(sportCheck);

        this.add(Box.createRigidArea(new Dimension(10,10)));

        Button buttonLaunch = new Button("Lancer la rumeur");
        buttonLaunch.addActionListener(launchListener);
        this.add(buttonLaunch);
    }

    private LaunchListener launchListener = new LaunchListener();
    public class LaunchListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            HashMap<String,Integer> centersOfInterest = new HashMap<String, Integer>();
            centersOfInterest.put(Constant.KEY_CINEMA, cineCheck.getState() ? 0 : 1);
            centersOfInterest.put(Constant.KEY_MUSIC, musicCheck.getState() ? 0 : 1);
            centersOfInterest.put(Constant.KEY_POLITIC, politiqueCheck.getState() ? 0 : 1);
            centersOfInterest.put(Constant.KEY_SPORT, sportCheck.getState() ? 0 : 1);

            try {
                RumorService.getInstance().spread(
                        new Rumor(
                                name,
                                (Integer) spinnerEditor.getModel().getNumber(),
                                centersOfInterest
                        ),
                        Constant.SPREAD_PREFERENCES
                );
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
