/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;

public class RumorJFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private static RumorJFrame rumorJFrame;

    public RumorJFrame() {
        this.placeComponents();
    }

    public static RumorJFrame getInstance(){
        if(rumorJFrame == null){
            rumorJFrame = new RumorJFrame();
        }
        return rumorJFrame;
    }

    public void placeComponents() {
        getContentPane().setLayout(new VerticalLayout());

        this.getContentPane().add(new StrategyPanel(this));

        /*Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height *//* * 1 / 4 *//*;
        int width = screenSize.width;
        this.setPreferredSize(new Dimension(width, height));*/
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setVisible(true);

    }


}
