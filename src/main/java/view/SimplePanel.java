/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import java.awt.Color;

import javax.swing.*;

public class SimplePanel extends JPanel {

    public SimplePanel() {

        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorSimplePanel(new Color(128,0,128), "Simple Violet"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorSimplePanel(Color.RED, "Simple Rouge"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorSimplePanel(Color.ORANGE, "Simple Orange"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorSimplePanel(Color.CYAN, "Simple Bleu"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorSimplePanel(Color.MAGENTA, "Simple Magenta"));
        this.add(Box.createHorizontalGlue());

    }

}
