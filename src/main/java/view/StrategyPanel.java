/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import service.RumorService;
import utils.Constant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class StrategyPanel extends JPanel {

    public static Button simpleButton;
    public static Button preferenceButton;
    public static Button centralityButton;
    public static Button msmButton;
    public static JTextField fileNameField;
    public static JSpinner lambdaSpinner;
    public static JSpinner.NumberEditor lambdaSpinnerEditor;
    public static JSpinner nbNodesSpinner;
    public static JSpinner.NumberEditor nbNodesSpinnerEditor;
    public static JLabel label = new JLabel("");
    public static JFrame parent;

    public StrategyPanel(JFrame pParent) {

        parent = pParent;

        this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));

        JPanel chooseStrategy = new JPanel();
        chooseStrategy.setLayout(new BoxLayout(chooseStrategy,BoxLayout.LINE_AXIS));
        simpleButton = new Button(Constant.SPREAD_SIMPLE);
        simpleButton.addActionListener(setStrategyListener);
        preferenceButton = new Button(Constant.SPREAD_PREFERENCES);
        preferenceButton.addActionListener(setStrategyListener);
        centralityButton = new Button(Constant.SPREAD_CENTRALITY);
        centralityButton.addActionListener(setStrategyListener);
        msmButton = new Button(Constant.SPREAD_MSM);
        msmButton.addActionListener(setStrategyListener);

        fileNameField = new JTextField();
        fileNameField.setMaximumSize(new Dimension(500,200));
        fileNameField.setPreferredSize(new Dimension(200,20));
        lambdaSpinner = new JSpinner(new SpinnerNumberModel(1.2,1.2,2,0.05));
        lambdaSpinnerEditor = new JSpinner.NumberEditor(lambdaSpinner);
        lambdaSpinner.setEditor(lambdaSpinnerEditor);
        nbNodesSpinner = new JSpinner(new SpinnerNumberModel(50,20,2000,20));
        nbNodesSpinnerEditor = new JSpinner.NumberEditor(nbNodesSpinner);
        nbNodesSpinner.setEditor(nbNodesSpinnerEditor);


        JPanel graphProps = new JPanel();
        graphProps.setLayout(new BoxLayout(graphProps, BoxLayout.LINE_AXIS));
        graphProps.add(Box.createHorizontalGlue());
        graphProps.add(new Label("Fichier"));
        graphProps.add(fileNameField);
        graphProps.add(Box.createHorizontalGlue());
        graphProps.add(new Label("Nb Noeuds"));
        graphProps.add(nbNodesSpinner);
        graphProps.add(Box.createHorizontalGlue());
        graphProps.add(new Label("Coef. Distrib"));
        graphProps.add(lambdaSpinner);
        graphProps.add(Box.createHorizontalGlue());

        this.add(Box.createRigidArea(new Dimension(10,30)));
        this.add(graphProps);
        this.add(Box.createRigidArea(new Dimension(10,30)));

        chooseStrategy.add(Box.createHorizontalGlue());
        chooseStrategy.add(simpleButton);
        chooseStrategy.add(Box.createHorizontalGlue());
        chooseStrategy.add(preferenceButton);
        chooseStrategy.add(Box.createHorizontalGlue());
        chooseStrategy.add(centralityButton);
        chooseStrategy.add(Box.createHorizontalGlue());
        chooseStrategy.add(msmButton);
        chooseStrategy.add(Box.createHorizontalGlue());
        this.add(chooseStrategy);


    }

    private SetStrategyListener setStrategyListener = new SetStrategyListener();
    public class SetStrategyListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            StrategyPanel.this.removeAll();

            JPanel chooseStrategy = new JPanel();
            String command = e.getActionCommand();
            label.setText(command + " Strategy");
            label.setFont(new Font(label.getFont().getName(),Font.PLAIN, 30));
            label.setHorizontalAlignment(SwingConstants.CENTER);
            chooseStrategy.setLayout(new BorderLayout());
            chooseStrategy.add(label,BorderLayout.CENTER);
            chooseStrategy.setBackground(Color.WHITE);

            StrategyPanel.this.add(Box.createHorizontalGlue());
            StrategyPanel.this.add(chooseStrategy);
            StrategyPanel.this.add(Box.createRigidArea(new Dimension(10,20)));

            File file = new File(fileNameField.getText());
            if(file.exists() && !file.isDirectory()) {
                RumorService.getInstance().displayGraph(fileNameField.getText(), Constant.MODE_RETRIEVE);
            } else {
                RumorService.getInstance().parameterGraph((Integer) nbNodesSpinnerEditor.getModel().getNumber(), (Double) lambdaSpinnerEditor.getModel().getNumber());
                RumorService.getInstance().displayGraph(fileNameField.getText(), Constant.MODE_SAVE);
            }

            if(command.equals(Constant.SPREAD_SIMPLE)) {
                StrategyPanel.this.add(new SimplePanel());
            } else if(command.equals(Constant.SPREAD_CENTRALITY)) {
                StrategyPanel.this.add(new CentralityPanel());
            } else if(command.equals(Constant.SPREAD_PREFERENCES)) {
                StrategyPanel.this.add(new PreferencePanel());
            } else if(command.equals(Constant.SPREAD_MSM)) {
                StrategyPanel.this.add(new MsmPanel());
            }

            StrategyPanel.this.add(Box.createHorizontalGlue());
            parent.pack();
        }
    }
}
