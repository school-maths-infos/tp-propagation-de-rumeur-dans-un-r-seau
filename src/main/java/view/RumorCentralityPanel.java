/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import model.Rumor;
import service.RumorService;
import utils.Constant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class RumorCentralityPanel extends JPanel {

    public JSpinner jspinLevelOfInterest;
    public JSpinner.NumberEditor spinnerEditor;
    public String name;
    public RumorCentralityPanel(Color color, String name) {

        this.name = name;

        this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        JPanel top = new JPanel();
        top.setBackground(color);
        top.add(new JLabel("..."));
        this.add(top);

        this.add(Box.createRigidArea(new Dimension(10,10)));

        JPanel degree = new JPanel();
        degree.setLayout(new BoxLayout(degree,BoxLayout.LINE_AXIS));
        degree.add(new JLabel("Degré d'importance : "));
        jspinLevelOfInterest = new JSpinner(new SpinnerNumberModel(0,0,5,1));
        spinnerEditor = new JSpinner.NumberEditor(jspinLevelOfInterest);
        jspinLevelOfInterest.setEditor(spinnerEditor);
        degree.add(jspinLevelOfInterest);
        this.add(degree);

        this.add(Box.createRigidArea(new Dimension(10,10)));

        Button buttonLaunch = new Button("Lancer la rumeur");
        buttonLaunch.addActionListener(launchListener);
        this.add(buttonLaunch);
    }

    private LaunchListener launchListener = new LaunchListener();
    public class LaunchListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                RumorService.getInstance().spread(
                        new Rumor(
                                name,
                                (Integer) spinnerEditor.getModel().getNumber(),
                                new HashMap()
                        ),
                        Constant.SPREAD_CENTRALITY
                );
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
