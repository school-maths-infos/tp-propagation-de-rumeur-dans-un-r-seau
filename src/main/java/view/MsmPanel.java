/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import javax.swing.*;
import java.awt.*;

public class MsmPanel extends JPanel {

    public MsmPanel() {

        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(new Color(128,0,128), "Msm Violet"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.RED, "Msm Rouge"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.ORANGE, "Msm Orange"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.CYAN, "Msm Bleu"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.MAGENTA, "Msm Magenta"));
        this.add(Box.createHorizontalGlue());
    }

}
