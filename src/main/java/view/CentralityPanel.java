/**
 * Juliana Leclaire
 * Céline de Roland
 *
 * INFO901 TP Propagation de rumeur
 */

package view;

import javax.swing.*;
import java.awt.*;

public class CentralityPanel extends JPanel {

    public CentralityPanel() {

        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(new Color(128,0,128), "Centrality Violet"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.RED, "Centrality Rouge"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.ORANGE, "Centrality Orange"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.CYAN, "Centrality Bleu"));
        this.add(Box.createHorizontalGlue());
        this.add(new RumorCentralityPanel(Color.MAGENTA, "Centrality Magenta"));
        this.add(Box.createHorizontalGlue());
    }

}
